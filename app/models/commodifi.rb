class Commodifi < ApplicationRecord
  SELLER = "seller"
  BUYER = "buyer"

  OIL_MEAL = "oil_meal"
  GRAINS = "grains"

  BUYER_PAYS_COMMAND = "check_buyer_pay_"
  SELLER_PAYS_COMMAND = "check_seller_pay_"


  GRAINS_LIST = [
    [{id: 1, text: '12.5 wheat', callback_data: 'price_trend1'}],
    [{id: 2, text: '11.5 wheat', callback_data: 'price_trend2'}],
    [{id: 3, text: 'Feed Wheat', callback_data: 'price_trend3'}],
    [{id: 4, text: 'Feed Barley', callback_data: 'price_trend4'}],
    [{id: 5, text: 'Feed Corn', callback_data: 'price_trend5'}],
    [{id: 6, text: 'Rapeseed', callback_data: 'price_trend6'}],
  ]

  OIL_LIST = [
    [{id: 1, text: 'Ukraine FOB Sunflower Oil', callback_data: 'price_trend1'}],
    [{id: 2, text: 'CIF Rotterdam Sunflower Oil', callback_data: 'price_trend2'}],
    [{id: 3, text: 'The Spread', callback_data: 'price_trend3'}],
    [{id: 4, text: 'Ukraine FOB Sunflower Meal', callback_data: 'price_trend4'}],
  ]
end
