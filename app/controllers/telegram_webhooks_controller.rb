class TelegramWebhooksController < Telegram::Bot::UpdatesController
  include Telegram::Bot::UpdatesController::MessageContext

  def start!(*)
    session[:checked_buyer_pay] = nil
    session[:checked_seller_pay] = nil
    respond_with :message, text: 'What would you like to do today?', reply_markup: {
      inline_keyboard: [
        [{text: 'Check Market News', callback_data: 'news'}],
        [{text: 'Look for buyer/seller', callback_data: 'commodifi'}],
      ],
    }
  end

  def help!(*)
    respond_with :message, text: t('.content')
  end

  def memo!(*args)
    if args.any?
      session[:memo] = args.join(' ')
      respond_with :message, text: t('.notice')
    else
      respond_with :message, text: t('.prompt')
      save_context :memo!
    end
  end

  def remind_me!(*)
    to_remind = session.delete(:memo)
    reply = to_remind || t('.nothing')
    respond_with :message, text: reply
  end

  def keyboard!(value = nil, *)
    if value
      respond_with :message, text: t('.selected', value: value)
    else
      save_context :keyboard!
      respond_with :message, text: t('.prompt'), reply_markup: {
        keyboard: [t('.buttons')],
        resize_keyboard: true,
        one_time_keyboard: true,
        selective: true,
      }
    end
  end

  def inline_keyboard!(*)
    respond_with :message, text: t('.prompt'), reply_markup: {
      inline_keyboard: [
        [
          {text: t('.alert'), callback_data: 'alert'},
          {text: t('.no_alert'), callback_data: 'no_alert'},
        ],
        [{text: t('.repo'), url: 'https://github.com/telegram-bot-rb/telegram-bot'}],
      ],
    }
  end

  def callback_query(data)
    if data.include? 'price_trend'
      price_trend!(data[('price_trend'.length)..-1])
    elsif data.include? Commodifi::BUYER_PAYS_COMMAND
      rewrite_buyer_pay_keyboard(data[Commodifi::BUYER_PAYS_COMMAND.length..-1])
    elsif data.include? Commodifi::SELLER_PAYS_COMMAND
      rewrite_seller_pay_keyboard(data[Commodifi::SELLER_PAYS_COMMAND.length..-1])
    else
      case data
      when 'save_buyer_pays'
        # do something with session[:checked_buyer_pay]
        full_prepayment!
      when 'save_seller_pays'
        # do something with session[:checked_seller_pay]
        fob_loading!
      when 'commodifi'
        commodifi!
      when 'news'
        news!
      when 'market_highlights'
        market_highlights!
      when 'commodity_prices'
        commodity_prices!
      when 'oil_meal_news'
        session[:news_mode] = Commodifi::OIL_MEAL
        oil_meal_news!
      when 'grains_news'
        session[:news_mode] = Commodifi::GRAINS
        grains_news!
      when 'trend'
        trend!
      when 'buyer'
        session[:mode] = Commodifi::BUYER
        products_list!
      when 'seller'
        session[:mode] = Commodifi::SELLER
        products_list!
      when 'grains'
        # some_function(params)
        grains!
      when 'oil'
        # some_function(params)
        oil!
      when 'oil_type'
        # some_function(params)
        oil_type!
      when 'packing_materials'
        # some_function(params)
        packing_materials!
      when 'oilseeds'
        # some_function(params)
        oilseeds!
      when 'wheat'
        # some_function(params)
        wheat!
      when 'protein_level'
        # some_function(params)
        protein_level!
      when 'fob_loading'
        # some_function(params)
        fob_loading!
      when 'full_prepayment'
        # some_function(params)
        full_prepayment!
      when 'delivery_term'
        # some_function(params)
        delivery_term!
      when 'set_delivery_term'
        # some_function(params)
        set_delivery_term!
      when 'payment_methods'
        # some_function(params)
        payment_methods!
      when 'rapeseed_oil'
        rapeseed_oil!
      when 'soyabean_oilarley'
        soyabean_oilarley!
      when 'bulk_or_bottled'
        bulk_or_bottled!
      when 'quantity'
        quantity!
        # some_function(params)
        # respond_with :message, text: "#{from['first_name']}, thx"
      when 'show_price'
        respond_with :message, text: show_price
      when 'result'
        # some_function(params)
        if session[:mode].eql? Commodifi::SELLER
          respond_with :message, text: "Thank you for your enquriy. We are looking for suitable buyers and will revert directly. "
        elsif session[:mode].eql? Commodifi::BUYER
          respond_with :message, text: "Thank you for your enquriy. We are looking for suitable sellers and will revert directly. "
        end
      when 'gap'
        puts ""
      else
        puts ""
      end
    end
  end

  # def message(message)
  #   respond_with :message, text: 'message'
  # end

  def inline_query(query, _offset)
    query = query.first(10) # it's just an example, don't use large queries.
    t_description = t('.description')
    t_content = t('.content')
    results = Array.new(5) do |i|
      {
        type: :article,
        title: "#{query}-#{i}",
        id: "#{query}-#{i}",
        description: "#{t_description} #{i}",
        input_message_content: {
          message_text: "#{t_content} #{i}",
        },
      }
    end
    answer_inline_query results
  end

  # As there is no chat id in such requests, we can not respond instantly.
  # So we just save the result_id, and it's available then with `/last_chosen_inline_result`.
  def chosen_inline_result(result_id, _query)
    session[:last_chosen_inline_result] = result_id
  end

  def last_chosen_inline_result!(*)
    result_id = session[:last_chosen_inline_result]
    if result_id
      respond_with :message, text: t('.selected', result_id: result_id)
    else
      respond_with :message, text: t('.prompt')
    end
  end

  def action_missing(action, *_args)
    if action_type == :command
      respond_with :message,
                   text: t('telegram_webhooks.action_missing.command', command: action_options[:command])
    else
      respond_with :message, text: t('telegram_webhooks.action_missing.feature', action: action)
    end
  end

  # def expire_date!(date)
  #   respond_with :message, text: "#{from['first_name']}, thx. Please put your 20$ deposit while we search for a suitable buyer. Incase we do not find a suitable buyer before the validity period expires, we guarantee to return the deposit"
  # end

  def q!(*args)
    # some_function(*args)
    if session[:mode].eql? Commodifi::SELLER
      months!
    elsif session[:mode].eql? Commodifi::BUYER
      respond_with :message, text: 'Which shipment period can you buy?
For setting the begining of shipment period input manual
"/ss MM/DD/YYYY"'
    end
  end

  def ss!(period)
    # some_function(period)
    end_ship_message
  end

  def end_ship_message
    respond_with :message, text: 'For setting the ending of shipment period input manual
"/es MM/DD/YYYY"'
  end

  def es!(period)
    # some_function(period)
    buyer_pay_options!
  end

  def place!(*args)
    # some_function(args.join(' '))
    respond_with :message, text: 'Do you have a price idea?
For set price per mt ($) input manual "/p price" or only "/p" if price are blank'
  end

  def p!(*args)
    if session[:mode].eql? Commodifi::BUYER
      # some_function(*args)
      title = 'Which Commodity origin are you buying?'
      commodity_origin!(title)
    elsif session[:mode].eql? Commodifi::SELLER
      # some_function(price)
      title = 'Which Commodity origin are you selling?'
      commodity_origin!(title)
    end
  end

  def port!(*args)
    # some_function(*args)
    indication_fob_price!
  end

  def january!
    payment_methods!
  end

  def february!
    payment_methods!
  end

  def march!
    payment_methods!
  end

  def april!
    payment_methods!
  end

  def may!
    payment_methods!
  end

  def june!
    payment_methods!
  end

  def july!
    payment_methods!
  end

  def august!
    payment_methods!
  end

  def september!
    payment_methods!
  end

  def october!
    payment_methods!
  end

  def november!
    payment_methods!
  end

  def december!
    payment_methods!
  end

  protected

  # news part
  def news!(*)
    respond_with :message, text: 'Which kind of market news would you like?', reply_markup: {
      inline_keyboard: [
        [{text: 'Market highlights', callback_data: 'market_highlights'}],
        [{text: 'Commodity Prices', callback_data: 'commodity_prices'}],
      ],
    }
  end

  def market_highlights!(*)
    respond_with :message, text: 'We are working on adding this section shortly!'
  end

  def oil_meal_news!(*)
    respond_with :message, text: 'Please select one:', reply_markup: {
      inline_keyboard: Commodifi::OIL_LIST
    }
  end

  def grains_news!(*)
    respond_with :message, text: 'Please select one:', reply_markup: {
      inline_keyboard: Commodifi::GRAINS_LIST,
    }
  end

  def commodity_prices!(*)
    respond_with :message, text: 'Which prices would you like?', reply_markup: {
      inline_keyboard: [
        [{text: 'Oil/Meal', callback_data: 'oil_meal_news'}],
        [{text: 'Grains', callback_data: 'grains_news'}],
      ],
    }
  end

  def price_trend!(data)
    if session[:news_mode].eql? Commodifi::GRAINS
      session[:product] = Commodifi::GRAINS_LIST.select {|item| item[0][:id] == data.to_i}[0][0][:text]
    elsif session[:news_mode].eql? Commodifi::OIL_MEAL
      case Commodifi::OIL_LIST.select {|item| item[0][:id] == data.to_i}[0][0][:text]
      when 'Ukraine FOB Sunflower Oil'
        excel_field = 'SUNOIL FOB Chorno'
      when 'CIF Rotterdam Sunflower Oil'
        excel_field = 'Sunflower oil CIF Rotterdam'
      when 'The Spread'
        excel_field = 'Spread'
      when 'Ukraine FOB Sunflower Meal'
        excel_field = 'Ukraine FOB Sunflower Meal'
      end
      session[:product] = excel_field
    end
    respond_with :message, text: 'Would you like to see the most recent price or a trend?', reply_markup: {
      inline_keyboard: [
        [{text: 'most recent price', callback_data: 'show_price'}],
        [{text: 'trend', callback_data: 'trend'}],
      ],
    }
  end

  def trend!(*)
    respond_with :message, text: 'Which trend would you like to see?', reply_markup: {
      inline_keyboard: [
        [{text: 'PAST 1 Months', url: 'https://trello-attachments.s3.amazonaws.com/5c53248b8012c469d461371d/5c5465773a1e137a44e5bd93/3b2b321330f41c4c2f157ee36f16eda9/image.png'}],
        [{text: 'PAST 3 Months', url: 'https://trello-attachments.s3.amazonaws.com/5c53248b8012c469d461371d/5c5465773a1e137a44e5bd93/3b2b321330f41c4c2f157ee36f16eda9/image.png'}],
        [{text: 'PAST 6 Months', callback_data: 'gap'}],
        [{text: 'PAST YEAR', callback_data: 'gap'}],
      ],
    }
  end

  # commodifi part
  def commodifi!(*)
    respond_with :message, text: 'Look for seller or buyer?', reply_markup: {
      inline_keyboard: [
        [{text: 'look for buyer', callback_data: 'seller'}],
        [{text: 'look for seller', callback_data: 'buyer'}],
      ],
    }
  end

  def products_list!
    if session[:mode].eql? Commodifi::BUYER
      title = 'Which category of products are you buying?'
      show_products!(title)
    elsif session[:mode].eql? Commodifi::SELLER
      title = 'Which category of products are you selling?'
      show_products!(title)
    end
  end

  def show_products!(title)
    respond_with :message, text: title, reply_markup: {
      inline_keyboard: [
        [{text: 'Grains', callback_data: 'grains'}],
        [{text: 'Oil', callback_data: 'oil'}],
      ]
    }
  end

  def grains!(*)
    show_grains!('Grains')
  end

  def show_grains!(title)
    respond_with :message, text: title, reply_markup: {
      inline_keyboard: [
        [{text: 'Wheat', callback_data: 'wheat'}],
        [{text: 'Corn', callback_data: 'quantity'}],
        [{text: 'Oilseeds', callback_data: 'oilseeds'}],
      ],
    }
  end

  def wheat!(*)
    show_wheat!('Wheat')
  end

  def show_wheat!(title)
    respond_with :message, text: title, reply_markup: {
      inline_keyboard: [
        [{text: 'Milling wheat', callback_data: 'protein_level'}],
        [{text: 'Feed wheat', callback_data: 'quantity'}],
      ],
    }
  end

  def protein_level!(*)
    show_protein_level!('Which protein level?')
  end

  def show_protein_level!(title)
    respond_with :message, text: title, reply_markup: {
      inline_keyboard: [
        [{text: '10,5%', callback_data: 'quantity'}],
        [{text: '11,5%', callback_data: 'quantity'}],
        [{text: '12.5%', callback_data: 'quantity'}],
        [{text: 'higher than 12.5%', callback_data: 'quantity'}],
      ],
    }
  end

  def oilseeds!(*)
    show_oilseeds!('Oilseeds')
  end

  def show_oilseeds!(title)
    respond_with :message, text: title, reply_markup: {
      inline_keyboard: [
        [{text: 'Soyabeans', callback_data: 'quantity'}],
        [{text: 'Sunflower seeds', callback_data: 'quantity'}],
        [{text: 'Rapeseeds', callback_data: 'quantity'}],
      ],
    }
  end

  def oil!(*)
    show_oil!('Oil')
  end

  def show_oil!(title)
    respond_with :message, text: title, reply_markup: {
      inline_keyboard: [
        [{text: 'Soyabeen Oil', callback_data: 'quantity'}],
        [{text: 'Rapeseed Oil', callback_data: 'quantity'}],
        [{text: 'Sunflower Oil', callback_data: 'oil_type'}],
      ],
    }
  end

  def oil_type!(*)
    show_oil_type!('Refined or Crude')
  end

  def show_oil_type!(title)
    respond_with :message, text: title, reply_markup: {
      inline_keyboard: [
        [{text: 'Refined', callback_data: 'quantity'}],
        [{text: 'Crude', callback_data: 'packing_materials'}],
      ],
    }
  end

  def packing_materials!(*)
    show_packing_materials!('bottled or bulk')
  end

  def show_packing_materials!(title)
    respond_with :message, text: title, reply_markup: {
      inline_keyboard: [
        [{text: 'Bottled', callback_data: 'quantity'}],
        [{text: 'Bulk', callback_data: 'quantity'}],
      ],
    }
  end

  # second part
  def quantity!(*)
    if session[:mode].eql? Commodifi::BUYER
      respond_with :message, text: 'Which quantity can you buy?
For set quantity input manual "/q quantity"'
    elsif session[:mode].eql? Commodifi::SELLER
      respond_with :message, text: 'What quantity can you sell?
For set quantity input manual "/q quantity"'
    end
  end

  def buyer_pay_options!
    pay_list = [
      {id: 1, name: 'Letter of Credit', checked: false},
      {id: 2, name: 'Cash Against Documents (originals)', checked: false},
      {id: 3, name: 'Cash Against Documents (Scan copies)', checked: false},
    ]
    unless session[:checked_buyer_pay]
      session[:checked_buyer_pay] = pay_list
    end
    board = []
    session[:checked_buyer_pay].each do |item|
      board.push([{text: (item[:checked] ? '✔ ' : ' ') + item[:name], callback_data: Commodifi::BUYER_PAYS_COMMAND + item[:id].to_s}])
    end
    respond_with :message, text: 'How can you pay? *select one or many', reply_markup: {
      inline_keyboard: board.push([{text: 'Send ➡', callback_data: 'save_buyer_pays'}]),
    }
  end

  def full_prepayment!(*)
    respond_with :message, text: 'Can you make a partial or full prepayment?', reply_markup: {
      inline_keyboard: [
        [{text: 'Yes', callback_data: 'delivery_term'}],
        [{text: 'No', callback_data: 'delivery_term'}],
      ],
    }
  end

  def delivery_term!(*)
    respond_with :message, text: 'On which delivery term are you buying?', reply_markup: {
      inline_keyboard: [
        [
          {text: 'CIF', callback_data: 'set_delivery_term'},
          {text: 'CFR', callback_data: 'set_delivery_term'},
          {text: 'FOB', callback_data: 'set_delivery_term'},
          {text: 'CPT', callback_data: 'set_delivery_term'},
          {text: 'FCA', callback_data: 'set_delivery_term'},
          {text: 'DAP', callback_data: 'set_delivery_term'},
        ],
      ],
    }
  end

  def set_delivery_term!(*)
    respond_with :message, text: 'Which port or place of delivery?
For set delivery term input manual "/place name"'
  end

  def commodity_origin!(title)
    respond_with :message, text: title, reply_markup: {
      inline_keyboard: [
        [{text: 'Ukraine', callback_data: 'result'}],
        [{text: 'Moldova', callback_data: 'result'}],
        [{text: 'Russia', callback_data: 'result'}],
        [{text: 'Any Black Sea', callback_data: 'result'}],
        [{text: 'EU', callback_data: 'result'}],
      ],
    }
  end

  def months!(*)
    respond_with :message, text: 'Which Month (months) can you offer?
/January
/February
/March
/April
/May
/June
/July
/August
/September
/October
/November
/December'
  end

  def payment_methods!(*)
    pay_list = [
      {id: 1, name: 'LC', checked: false},
      {id: 2, name: '100% Cash Against', checked: false},
      {id: 3, name: 'Against', checked: false},
      {id: 4, name: 'Documents', checked: false},
      {id: 5, name: 'Partial Prepayment', checked: false},
      {id: 6, name: 'Full prepayment', checked: false}
    ]
    unless session[:checked_seller_pay]
      session[:checked_seller_pay] = pay_list
    end
    board = []
    session[:checked_seller_pay].each do |item|
      board.push([{text: (item[:checked] ? '✔ ' : ' ') + item[:name], callback_data: Commodifi::SELLER_PAYS_COMMAND + item[:id].to_s}])
    end
    respond_with :message, text: 'Which Payment methods do you accept? Select one or all', reply_markup: {
      inline_keyboard: board.push([{text: 'Send ➡', callback_data: 'save_seller_pays'}]),
    }
  end

  def fob_loading!(*)
    respond_with :message, text: 'What is your FOB loading port?
For set FOB loading port input manual "/port name"'
  end

  def indication_fob_price!(*)
    respond_with :message, text: 'Can you please give an indication of your FOB price?
For set price per mt ($) input manual "/p price" or only "/p" if price are blank'
  end

  def rewrite_buyer_pay_keyboard(data)
    session[:checked_buyer_pay].each do |item|
      if item[:id] == data.to_i
        item[:checked] = !item[:checked]
      end
    end

    board = []
    session[:checked_buyer_pay].each do |item|
      board.push([{text: (item[:checked] ? '✔ ' : ' ') + item[:name], callback_data: Commodifi::BUYER_PAYS_COMMAND + item[:id].to_s}])
    end

    bot.edit_message_text(
      chat_id: payload['message']['chat']['id'],
      message_id: payload['message']['message_id'],
      text: "How can you pay? *select one or many",
      reply_markup: {
        inline_keyboard: board.push([{text: 'Send ➡', callback_data: 'save_buyer_pays'}])
      }
    )
  end

  def rewrite_seller_pay_keyboard(data)
    session[:checked_seller_pay].each do |item|
      if item[:id] == data.to_i
        item[:checked] = !item[:checked]
      end
    end

    board = []
    session[:checked_seller_pay].each do |item|
      board.push([{text: (item[:checked] ? '✔ ' : ' ') + item[:name], callback_data: Commodifi::SELLER_PAYS_COMMAND + item[:id].to_s}])
    end

    bot.edit_message_text(
      chat_id: payload['message']['chat']['id'],
      message_id: payload['message']['message_id'],
      text: "Which Payment methods do you accept? Select one or all",
      reply_markup: {
        inline_keyboard: board.push([{text: 'Send ➡', callback_data: 'save_seller_pays'}])
      }
    )
  end

  def show_price
    if session[:news_mode].eql? Commodifi::GRAINS
      doc = SimpleXlsxReader.open(Rails.public_path.join('tmp/ukraine_fob.xlsx'))
      doc.sheets.first.rows.each_with_index do |row, index|
        row.each_with_index do |cell, i|
          if cell
            if cell.to_s.downcase.strip.eql?session[:product].downcase
              start_row = index + 2
              doc.sheets.first.rows.each do |r|
                if doc.sheets.first.rows[start_row][i]
                  return cell.to_s + ': ' + doc.sheets.first.rows[start_row][i-1].strftime("%d.%m.%Y") + ', Mid Price Low: ' + doc.sheets.first.rows[start_row][i].to_s + ', Mid Price High: ' + doc.sheets.first.rows[start_row][i + 1].to_s
                end
                start_row += 1
              end
            end
          end
        end
      end
    elsif session[:news_mode].eql? Commodifi::OIL_MEAL
      doc = SimpleXlsxReader.open(Rails.public_path.join('tmp/sunoil_spread.xlsx'))
      doc.sheets.first.rows.each_with_index do |row, index|
        row.each_with_index do |cell, i|
          if cell
            if cell.to_s.downcase.strip.eql?session[:product].downcase
              start_row = index + 1
              doc.sheets.first.rows.each do |r|
                if doc.sheets.first.rows[start_row][i]
                  return cell.to_s + ': ' + doc.sheets.first.rows[start_row][i-1].strftime("%d.%m.%Y") + ', Price:' + doc.sheets.first.rows[start_row][i].to_s
                end
                start_row += 1
              end
            end
          end
        end
      end
    end

    return 'no actual result'
  end

  def some_function!(params)
    # session[:mode]
  end
end
